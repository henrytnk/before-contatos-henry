<!DOCTYPE html>
<html>

@include('partials._header')

<body>
	
	@include('partials._navbar')

	@yield('jumbotron')

	<div class="container">
		
		@include('partials._alert')

		@yield('content')

	</div>

	@include('partials._footer')

	@include('partials._script')

</body>
</html>