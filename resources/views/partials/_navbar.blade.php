<div class="nav-scroller bg-white box-shadow">
    <nav class="nav nav-underline">
        <a class="nav-link active" href="{{ route('pages.home') }}">Henry Tanaka</a>
        <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="{{ route('pages.home') }}">Home</a>
        <a class="nav-link {{ (Request::is('contatos') || Request::is('search')) ? 'active' : '' }}" href="{{ route('contatos.index') }}">
            Contatos
            <span class="badge badge-pill bg-light align-text-bottom">{{ $nCon }}</span>
        </a>
        <a class="nav-link {{ Request::is('portofolio') ? 'active' : '' }}" href="{{ route('pages.portofolio') }}">Portfólio</a>
    </nav>
</div>
