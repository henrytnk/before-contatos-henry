@extends('main')

@section('pageTitle', '| Portfólio')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<h1 class="display-4">Portfólio</h1>
			<p class="lead">Conheça um pouco de meus trabalhos anteriores.</p>
		</div>
		<hr>
		<div class="col-md-2">
			<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				<i class="fas fa-arrow-left" style="color: black;"></i>
				<span class="sr-only">Anterior</span>
			</a>
		</div>
		<div id="carouselExampleControls" class="col-md-8 text-center carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="../portfolio/birding.png">
					<a href="http://www.birdingbr.com.br/" target="_blank" class="h5">
						<span class="portfolio carousel-caption d-none d-md-block">
							<h5>Birding BR</h5>
							<small>&copy;Gestão Ativa 2016</small>
						</span>
					</a>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="../portfolio/dwo.png">
					<a href="http://www.dwo.com.br/" target="_blank" class="h5">
						<span class="portfolio carousel-caption d-none d-md-block">
							<h5>DWO Odontologia</h5>
							<small>&copy;Gestão Ativa 2016</small>
						</span>
					</a>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="../portfolio/reobote.png">
					<a href="http://reoboteconsorcios.com.br/" target="_blank" class="h5">
						<span class="portfolio carousel-caption d-none d-md-block">
							<h5>Reobote Consórcios</h5>
							<small>&copy;Gestão Ativa 2016</small>
						</span>
					</a>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="../portfolio/marlon.png">
					<a href="http://marlonvolei.com.br" target="_blank" class="h5">
						<span class="portfolio carousel-caption d-none d-md-block">
							<h5>Marlon Vôlei</h5>
							<small>&copy;Gestão Ativa 2015</small>
						</span>
					</a>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="../portfolio/funlec.png">
					<a href="http://funlec.com.br" target="_blank" class="h5">
						<span class="portfolio carousel-caption d-none d-md-block">
							<h5>FUNLEC</h5>
							<small>&copy;Funlec 2017</small>
						</span>
					</a>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="../portfolio/osorio.png">
					<a href="http://egocg.com.br/" target="_blank" class="h5">
						<span class="portfolio carousel-caption d-none d-md-block">
							<h5>Colégio General Osório</h5>
							<small>&copy;Gestão Ativa 2016</small>
						</span>
					</a>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="../portfolio/pimentel.png">
					<a href="http://rodrigopimentel.com.br/" target="_blank" class="h5">
						<span class="portfolio carousel-caption d-none d-md-block">
							<h5>Rodrigo Pimentel</h5>
							<small>&copy;Gestão Ativa 2015</small>
						</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				<i class="fas fa-arrow-right" style="color: black;"></i>
				<span class="sr-only">Próximo</span>
			</a>
		</div>
	</div>

@endsection
