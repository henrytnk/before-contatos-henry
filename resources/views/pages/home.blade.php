@extends('main')

@section('title', '| Homepage')

@section('jumbotron')

	<div class="jumbotron">
		<div class="container">
			<h1 class="display-3">Bem-vindo!</h1>
			<p>Esta é a homepage de minha aplicação web. Esta prova foi desenvolvida utilizando os frameworks <strong>Laravel 5.6</strong> e <strong>Vue.js 2.0</strong>. Os testes automatizados foram feitos utilizando o <strong>Laravel Dusk</strong>. Clique no botão abaixo para ser redirecionado à página da aplicação!</p>
			<p><a class="btn btn-primary btn-lg" href="{{ route('contatos.index') }}" role="button">Ir para aplicação »</a></p>
		</div>
	</div>

@endsection
