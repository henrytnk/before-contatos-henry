@extends('main')

@section('pageTitle', '| Lista de Contatos')

@section('content')

	<div class="row">
		<div class="col-md-10 offset-md-1 bg-white rounded box-shadow">
				<div class="pagetitle">
					<h2 class="display-4 pb-2 mb-0">Lista de Contatos</h2>
					<a href="{{ route('contatos.create') }}" class="btn btn-success novo_contato"><i class="fa fas fa-plus-circle"></i> Adicionar</a>
				</div>
				<div class="buscaNav flex-md-nowrap p-0">
					{!! Form::open(['route' => 'contatos.search', 'id' => 'searchForm', 'class' => 'hero__form', 'method' => 'GET']) !!}
	        		<div class="form-group">
	            		<div class="form-group--search form-group--search--left">
											{{ Form::text('busca', $busca, ['class' => 'form-control form-control-dark w-100', 'placeholder' => 'Busca']) }}
											<button type="submit" class="btn-submit"><i class="fa fa-search"></i></button>
		              </div>
		          </div>
					{!! Form::close() !!}
				</div>
				<div class="contatos">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Nome</th>
								<th scope="col">Telefone</th>
								<th scope="col">E-mail</th>
								<th scope="col">Ações</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							@foreach($contatos as $contato)
								<tr>
									<td scope="row">{{ $contato->nome }}</td>
									<td>{{ $contato->telefone }}</td>
									<td>{{ $contato->email }}</td>
									<td>
											<!-- {{ Html::linkRoute('contatos.edit', 'Editar', $contato->id, ['class' => 'btn btn-primary btn-sm btn-block']) }} -->
											<a href="{{ route('contatos.edit', $contato->id) }}" class="btn btn-primary btn-sm btn-block"><i class="fa far fa-edit"></i> Editar</a>
									</td>
									<td>
										<a href="{{ route('contatos.delete', $contato->id) }}" class="confirmDelete btn btn-danger btn-sm btn-block"><i class="fas fa-minus-circle"></i> Deletar</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>

					{{-- Paginacao --}}
					<div class="text-center">
						{!! $contatos->links() !!}
					</div>

					@if($busca)

						<div class="return text-center">
							{{ Html::linkRoute('contatos.index', '< Voltar para o index') }}
						</div>

					@endif
				</div>
		</div>
	</div>

@endsection

@section('javascript')

	$('.confirmDelete').on('click', function () {
			return confirm('Tem certeza de que deseja remover este contato?');
	});

@endsection
