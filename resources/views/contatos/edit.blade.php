@extends('main')

@section('pageTitle', '| Editar Contato')

@section('content')

	{!! Form::model($contato, ['route' => ['contatos.update', $contato->id], 'id' => 'editForm', 'v-on:submit' => 'validarForm', 'method' => 'PUT']) !!}
	<div class="row">
		<div class="col-md-10 col-offset-2 bg-white rounded box-shadow">
			<div class="alert alert-danger" v-if="errors.length">
				<b>Os seguintes erros foram encontrados:</b>
				<ul>
					<li v-for="error in errors" v-cloak>@{{ error }}</li>
				</ul>
			</div>
			<h2 class="display-4 border-bottom border-gray pb-2 mb-0">Editar Contato</h2>
			<div class="form-group">
				{{ Form::label('nome', 'Nome:', ['class' => 'lead']) }}
				{{ Form::text('nome', null, ['class' => 'awesome form-control', 'v-model' => 'nome']) }}
			</div>
			<div class="form-group">
				{{ Form::label('telefone', 'Telefone:', ['class' => 'lead']) }}
				{{ Form::text('telefone', null, ['class' => 'awesome form-control', 'v-model' => 'telefone']) }}
			</div>
			<div class="form-group">
				{{ Form::label('email', 'E-Mail:', ['class' => 'lead']) }}
				{{ Form::text('email', null, ['class' => 'awesome form-control', 'v-model' => 'email']) }}
			</div>
			{{ Html::linkRoute('contatos.index', 'Cancelar', null, ['class' => 'btn btn-danger']) }}
			{{ Form::submit('Salvar Alterações', ['class' => 'btn btn-success']) }}
		</div>
	</div>
	{!! Form::close() !!}

@endsection

@section('javascript')

	new Vue({
		el:'#editForm',
		data:{
			errors:[],
			nome:'{{ $contato->nome }}',
			email:'{{ $contato->email }}',
			telefone:'{{ $contato->telefone }}'
		},
		methods:{
			validarForm:function(e) {
				if(this.nome && this.email && this.telefone) return true;
				this.errors = [];
				if(!this.nome) this.errors.push("Campo [nome] obrigatório.");
				if(!this.email) this.errors.push("Campo [e-mail] obrigatório.");
				if(!this.telefone) this.errors.push("Campo [telefone] obrigatório.");
				e.preventDefault();
			}
		}
	})

@endsection