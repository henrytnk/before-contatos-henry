@extends('main')

@section('pageTitle', '| Adicionar Contato')

@section('content')

	{!! Form::open(['route' => 'contatos.store', 'id' => 'createForm', 'v-on:submit' => 'validarForm']) !!}
	<div class="row">
		<div class="col-md-10 col-offset-2 bg-white rounded box-shadow">
			<div class="alert alert-danger" v-if="errors.length">
				<b>Os seguintes erros foram encontrados:</b>
				<ul>
					<li v-for="error in errors" v-cloak>@{{ error }}</li>
				</ul>
			</div>
			<h2 class="display-4 border-bottom border-gray pb-2 mb-0">Adicionar Contato</h2>
			<div class="form-group">
				{{ Form::label('nome', 'Nome:', ['class' => 'lead']) }}
				{{ Form::text('nome', '', ['class' => 'awesome form-control', 'v-model' => 'nome']) }}
			</div>
			<div class="form-group">
				{{ Form::label('telefone', 'Telefone:', ['class' => 'lead']) }}
				{{ Form::text('telefone', '', ['class' => 'awesome form-control', 'v-model' => 'telefone']) }}
			</div>
			<div class="form-group">
				{{ Form::label('email', 'E-Mail:', ['class' => 'lead']) }}
				{{ Form::text('email', '', ['class' => 'awesome form-control', 'v-model' => 'email']) }}
			</div>
			{{ Form::submit('Criar Contato', ['class' => 'btn btn-success', 'dusk' => 'registrar-btn']) }}
		</div>
	</div>
	{!! Form::close() !!}

@endsection

@section('javascript')

	new Vue({
		el:'#createForm',
		data:{
			errors:[],
			nome:null,
			email:null,
			telefone:null
		},
		methods:{
			validarForm:function(e) {
				if(this.nome && this.email && this.telefone) return true;
				this.errors = [];
				if(!this.nome) this.errors.push("Campo [nome] obrigatório.");
				if(!this.email) this.errors.push("Campo [e-mail] obrigatório.");
				if(!this.telefone) this.errors.push("Campo [telefone] obrigatório.");
				e.preventDefault();
			}
		}
	})

@endsection
