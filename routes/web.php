<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@getHome')->name('pages.home');
Route::get('/portofolio', 'PagesController@getPortfolio')->name('pages.portofolio');
Route::resource('contatos', 'ContatosController')->except(['destroy']);
Route::get('contatos/{id}/delete', ['uses' => 'ContatosController@destroy', 'as' => 'contatos.delete']);
Route::get('/search', ['uses' => 'ContatosController@search', 'as' => 'contatos.search']);
