<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testeRegistro()
    {
        $this->browse(function (Browser $browser) {
          $browser->visit('http://localhost:8000/contatos');

          for ($i=1; $i <= 7; $i++) {
            $browser->clickLink('Adicionar')
                    ->type('#nome', 'Contato ' . $i)
                    ->type('#telefone', '(67) 9' . rand(1111, 9999) . '-' . rand(1111, 9999))
                    ->type('#email', 'contato' . $i . '@hotmail.com')
                    ->click('@registrar-btn')
                    ->assertSee('Lista de Contatos');
          }
        });
    }
}
