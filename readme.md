## Prova de Contatos

Esta aplicação foi feita utilizando os frameworks [Laravel 5.6](http://laravel.com) e [Vue.js](https://vuejs.org). 

## Instruções para rodar a aplicação

* Faça o download do repositório
* Configure o arquivo .env e /config/database.php com os dados de sua database
* Na pasta root do projeto, execute o comando ```composer install```
* A seguir, execute o comando ```php artisan migrate```
* Por fim, execute o comando ```php artisan serve``` em seu terminal e acesse a aplicação pelo browser

## Instruções para teste automatizado

Para executar os testes automatizados utilizando o Laravel Dusk, basta executar o comando ```php artisan dusk``` na pasta root do projeto, e uma lista de contatos será gerada automaticamente.