<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contato;
use Session;

class ContatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contatos = Contato::orderBy('nome', 'ASC')->paginate(5);

        return view('contatos.index')->withContatos($contatos)->withBusca(null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contatos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|max:255',
            'telefone' => 'required|max:255',
            'email' => 'required|max:255'
        ]);

        $contato = new Contato;
        $contato->nome = $request->input('nome');
        $contato->telefone = $request->input('telefone');
        $contato->email = $request->input('email');
        $contato->save();

        Session::flash('success', 'Novo contato salvo com sucesso!');

        return redirect()->route('contatos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contato = Contato::find($id);

        return view('contatos.edit')->withContato($contato);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required|max:255',
            'telefone' => 'required|max:255',
            'email' => 'required|max:255'
        ]);

        $contato = Contato::find($id);
        $contato->nome = $request->input('nome');
        $contato->telefone = $request->input('telefone');
        $contato->email = $request->input('email');
        $contato->save();

        Session::flash('success', 'Contato atualizado com sucesso.');

        return redirect()->route('contatos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contato = Contato::find($id);
        $contato->delete();

        Session::flash('success', 'Contato removido com sucesso.');

        return redirect()->route('contatos.index');
    }

    public function search(Request $request)
    {
        $busca = $request->input('busca');

        if($busca) {
            $contatos = Contato::where('nome', 'LIKE', '%' . $busca . '%')
                                ->orWhere('telefone', 'LIKE', '%' . $busca . '%')
                                ->orWhere('email', 'LIKE', '%' . $busca . '%')
                                ->paginate(5);
            $contatos->appends(['busca' => $busca]);

            return view('contatos.index')->withContatos($contatos)->withBusca($busca);
        } else {
            return redirect()->route('contatos.index');
        }
    }
}
